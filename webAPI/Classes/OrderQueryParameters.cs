﻿using System;
namespace webAPI.Classes
{
    public class OrderQueryParameters : Query
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateOrderPlaced { get; set; }
        public string Details { get; set; }
    }
}
