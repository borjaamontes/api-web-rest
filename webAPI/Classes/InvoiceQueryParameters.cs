﻿using System;
namespace webAPI.Classes
{
    public class InvoiceQueryParameters : Query
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string Details { get; set; }
        public DateTime Date { get; set; }
    }
}
