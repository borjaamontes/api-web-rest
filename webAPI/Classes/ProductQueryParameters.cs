﻿using System;
using System.Threading.Tasks;
using System.Linq;
namespace webAPI.Classes
{
    public class ProductQueryParameters : Query
    {
        public string Sku { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MaxPrice { get; set; }
        public string Name { get; set; }
    }
}
