﻿using System;
namespace webAPI.Classes
{
    public class ShipmentQueryParameters : Query
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int InvoiceId { get; set; }
    }
}
