﻿using System;
namespace webAPI.Classes
{
    public class Query
    {
        const int _maxSize = 50;
        private int _size = 25;
        private string _sortOrder = "asc";

        public int Page { get; set; }
        public int Size { get { return _size; } set { _size = Math.Min(_maxSize, value); } }
        public string SortBy { get; set; } = "Id";
        public string SortOrder{get { return _sortOrder;} set { if (value == "asc" || value == "desc") { _sortOrder = value; } }          
        }
    }
}
