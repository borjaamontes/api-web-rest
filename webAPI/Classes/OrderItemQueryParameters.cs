﻿using System;
namespace webAPI.Classes
{
    public class OrderItemQueryParameters : Query
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double RMANumber { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
    }
}
