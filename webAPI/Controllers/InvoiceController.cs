﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webAPI.Classes;
using webAPI.Models;

namespace webAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("invoice")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly MyDbContext _context;

        public InvoiceController(MyDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllInvoices([FromQuery] InvoiceQueryParameters query)

        {
            IQueryable<Invoice> invoices = _context.Invoices;

            var c = invoices.Count();


            if (!string.IsNullOrEmpty(query.SortBy))
            {
                if (typeof(Customer).GetProperty(query.SortBy) != null)
                {
                    invoices = invoices.OrderByCustom(query.SortBy, query.SortOrder);
                }
            }

            invoices = invoices.Skip(query.Size * (query.Page - 1)).Take(query.Size);
            return Ok(await invoices.ToArrayAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetInvoice(int id)
        {
            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }
            return Ok(invoice);
        }

        [HttpPost]
        public async Task<ActionResult<Invoice>> InsertInvoice([FromBody] Invoice invoice)
        {
            _context.Invoices.Add(invoice);
            await _context.SaveChangesAsync();
            return CreatedAtAction(
                "GetInvoice",
                new { id = invoice.Id },
                invoice
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Invoice>> UpdateInvoice(int id, [FromBody] Invoice invoice)
        {

            if (id != invoice.Id)
            {
                return BadRequest();
            }

            _context.Entry(invoice).State = EntityState.Modified;

            try { await _context.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Invoices.Find(id) == null)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Invoice>> DeleteInvoice(int id)
        {
            var invoice = await _context.Invoices.FindAsync(id);
            if (invoice == null)
            {
                return NotFound();
            }

            _context.Invoices.Remove(invoice);
            await _context.SaveChangesAsync();
            return invoice;
        }
    }
}