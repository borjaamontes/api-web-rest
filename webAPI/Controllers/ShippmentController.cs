﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webAPI.Classes;
using webAPI.Models;

namespace webAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("shipment")]
    [ApiController]
    public class ShipmentController : ControllerBase
    {
        private readonly MyDbContext _context;

        public ShipmentController(MyDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllShipments([FromQuery] ShipmentQueryParameters query)

        {
            IQueryable<Shipment> shipments = _context.Shipments;

            var c = shipments.Count();


            if (!string.IsNullOrEmpty(query.SortBy))
            {
                if (typeof(Order).GetProperty(query.SortBy) != null)
                {
                    shipments = shipments.OrderByCustom(query.SortBy, query.SortOrder);
                }
            }

            shipments = shipments.Skip(query.Size * (query.Page - 1)).Take(query.Size);
            return Ok(await shipments.ToArrayAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetShipment(int id)
        {
            var shipment = await _context.Shipments.FindAsync(id);
            if (shipment == null)
            {
                return NotFound();
            }
            return Ok(shipment);
        }

        [HttpPost]
        public async Task<ActionResult<Shipment>> Insertshipment([FromBody] Shipment shipment)
        {
            _context.Shipments.Add(shipment);
            await _context.SaveChangesAsync();
            return CreatedAtAction(
                "GetShipments",
                new { id = shipment.Id },
                shipment
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Shipment>> UpdateShipment(int id, [FromBody] Shipment shipment)
        {

            if (id != shipment.Id)
            {
                return BadRequest();
            }

            _context.Entry(shipment).State = EntityState.Modified;

            try { await _context.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Shipments.Find(id) == null)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Shipment>> DeleteShipment(int id)
        {
            var shipment = await _context.Shipments.FindAsync(id);
            if (shipment == null)
            {
                return NotFound();
            }

            _context.Shipments.Remove(shipment);
            await _context.SaveChangesAsync();
            return shipment;
        }
    }
}