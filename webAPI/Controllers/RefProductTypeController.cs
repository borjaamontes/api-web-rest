﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webAPI.Classes;
using webAPI.Models;

namespace webAPI.Controllers
{
    [ApiVersion("1.0")]
    //[Route("v{v:apiVersion}/product")]
    [Route("refProductType")]
    [ApiController]
    public class RefProductTypeController : ControllerBase
    {
        private readonly MyDbContext _context;

        public RefProductTypeController(MyDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpGet]
         public async Task<IActionResult> GetAllRefProductTypes([FromQuery] RefProductTypesQueryParameters query)
        //public async Task<IActionResult> GetAllRefProductTypes()
    
            {
            IQueryable<RefProductType> refProductTypes = _context.RefProductTypes;

            var c = refProductTypes.Count();


            if (!string.IsNullOrEmpty(query.Description))
            {
                refProductTypes = refProductTypes.Where(p => p.Description == query.Description);
            }

            if (!string.IsNullOrEmpty(query.SortBy))
            {
                if (typeof(Product).GetProperty(query.SortBy) != null)
                {
                    refProductTypes = refProductTypes.OrderByCustom(query.SortBy, query.SortOrder);
                }
            }

            refProductTypes = refProductTypes.Skip(query.Size * (query.Page - 1)).Take(query.Size);
            return Ok(await refProductTypes.ToArrayAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetRefProductType(int id)
        {
            var refProductType = await _context.RefProductTypes.FindAsync(id);
            if (refProductType == null)
            {
                return NotFound();
            }
            return Ok(refProductType);
        }

        [HttpPost]
        public async Task<ActionResult<RefProductType>> InsertRefProductType([FromBody] RefProductType refProductType)
        {
            _context.RefProductTypes.Add(refProductType);
            await _context.SaveChangesAsync();
            return CreatedAtAction(
                "GetRefProductType",
                new { id = refProductType.Id },
                refProductType
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<RefProductType>> UpdateRefProductType(int id, [FromBody] RefProductType refProductType)
        {
            string aa = refProductType.Description;
            var bb = aa;
            if (id != refProductType.Id)
            {
                return BadRequest();
            }

            _context.Entry(refProductType).State = EntityState.Modified;

            try { await _context.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.RefProductTypes.Find(id) == null)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<RefProductType>> DeleteRefProductType(int id)
        {
            var refProductType = await _context.RefProductTypes.FindAsync(id);
            if (refProductType == null)
            {
                return NotFound();
            }

            _context.RefProductTypes.Remove(refProductType);
            await _context.SaveChangesAsync();
            return refProductType;
        }
    }
}