using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webAPI.Classes;
using webAPI.Models;

namespace webAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly MyDbContext _context;

        public ProductController(MyDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpGet]
        [Route("GetProducts")]
        public async Task<IActionResult> GetAllProducts([FromQuery] ProductQueryParameters query)
        {
            IQueryable<Product> products = _context.Products.Where(p => p.isAvailable == true);

            var c = products.Count();
            if (query.MinPrice != null &&
                            query.MaxPrice != null)
            {
                products = products.Where(
                    p => p.Price >= query.MinPrice.Value &&
                         p.Price <= query.MaxPrice.Value);
            }
            if (!string.IsNullOrEmpty(query.Sku))
            {
                products = products.Where(p => p.Sku == query.Sku);
            }

            if (!string.IsNullOrEmpty(query.Name))
            {
                products = products.Where(z => z.Name.ToUpper().Contains(query.Name.ToUpper()));

            }

            if (!string.IsNullOrEmpty(query.SortBy))
            {
                if (typeof(Product).GetProperty(query.SortBy) != null)
                {
                    products = products.OrderByCustom(query.SortBy, query.SortOrder);
                }
            }

            products = products.Skip(query.Size * (query.Page - 1)).Take(query.Size);
            return Ok(await products.ToArrayAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        [HttpGet]
        [Route("name/{name}")]
        public async Task<IActionResult> GetProductByName(string name)
        {
            IQueryable<Product> products = _context.Products.Where(z => z.Name.ToLower().Contains(name.ToLower()) && z.isAvailable==true);
            var c = products.Count();

            if (products == null)
            {
                return NotFound();
            }
            return Ok(await products.ToArrayAsync());
        }

        [HttpPost]
        public async Task<ActionResult<Product>> InsertProduct([FromBody] Product product)
        {
            _context.Products.Add(product);
            await _context.SaveChangesAsync();
            return CreatedAtAction(
                "GetProduct",
                new { id = product.Id },
                product
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Product>> UpdateProduct (int id, [FromBody] Product product)
        {
            if (id!= product.Id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try { await _context.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.Products.Find(id) == null)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct (int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product== null)
            {
                return NotFound();
            }

             _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return product;
        }
    }
}