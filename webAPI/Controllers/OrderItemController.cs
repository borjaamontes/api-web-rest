﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webAPI.Classes;
using webAPI.Models;

namespace webAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("orderItem")]
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        private readonly MyDbContext _context;

        public OrderItemController(MyDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrderItems([FromQuery] OrderItemQueryParameters query)

        {
            IQueryable<OrderItem> orderItems = _context.OrderItems;

            var c = orderItems.Count();


            if (!string.IsNullOrEmpty(query.SortBy))
            {
                if (typeof(OrderItem).GetProperty(query.SortBy) != null)
                {
                    orderItems = orderItems.OrderByCustom(query.SortBy, query.SortOrder);
                }
            }

            orderItems = orderItems.Skip(query.Size * (query.Page - 1)).Take(query.Size);
            return Ok(await orderItems.ToArrayAsync());
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetOrderItem(int id)
        {
            var orderItem = await _context.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return NotFound();
            }
            return Ok(orderItem);
        }

        [HttpPost]
        public async Task<ActionResult<OrderItem>> InsertOrderItem([FromBody] OrderItem orderItem)
        {
            _context.OrderItems.Add(orderItem);
            await _context.SaveChangesAsync();
            return CreatedAtAction(
                "GetOrderItem",
                new { id = orderItem.Id },
                orderItem
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<OrderItem>> UpdateOrderItem(int id, [FromBody] OrderItem orderItem)
        {

            if (id != orderItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(orderItem).State = EntityState.Modified;

            try { await _context.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (_context.OrderItems.Find(id) == null)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<OrderItem>> DeleteOrderItem(int id)
        {
            var orderItem = await _context.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return NotFound();
            }

            _context.OrderItems.Remove(orderItem);
            await _context.SaveChangesAsync();
            return orderItem;
        }
    }
}