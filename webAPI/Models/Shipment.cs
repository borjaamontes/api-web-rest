﻿using System;
using System.Text.Json.Serialization;

namespace webAPI.Models
{
    public class Shipment
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        [JsonIgnore]
        public virtual Order Order { get; set; }
        public int InvoiceId { get; set; }
        [JsonIgnore]
        public virtual Invoice Invoice { get; set; }
    }
}
