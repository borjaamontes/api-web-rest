﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace webAPI.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string Details { get; set; }
        public DateTime Date { get; set; }
        [JsonIgnore]
        public virtual Order Order { get; set; }
        public virtual List<Shipment>Shipments { get; set; }
    }
}
