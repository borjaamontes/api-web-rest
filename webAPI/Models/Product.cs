﻿using System;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace webAPI.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Description { get; set; }
        public bool isAvailable { get; set; }
        public int ProductTypeId { get; set; }
        public string Image { get; set; }
        [JsonIgnore]
        public virtual RefProductType RefProductType { get; set; }

    }
}

