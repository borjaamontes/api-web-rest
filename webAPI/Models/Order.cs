﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace webAPI.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateOrderPlaced { get; set; }
        public string Details { get; set; }
        [JsonIgnore]
        public virtual Customer Customer { get; set; }
        public virtual List<OrderItem> OrderItems { get; set; }
        public virtual List<Shipment> Shipments { get; set; }
        public virtual List<Invoice> Invoices { get; set; }
    }
}
