﻿using System;
using System.Collections.Generic;

namespace webAPI.Models
{
    public class RefProductType
    {
        public RefProductType()
        {
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public virtual List<Product> Products {get; set;}
    }
}
