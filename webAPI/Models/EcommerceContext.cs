﻿using System;
using Microsoft.EntityFrameworkCore;

namespace webAPI.Models
{
    public class EcommerceContext : DbContext
    {
        public EcommerceContext(DbContextOptions<EcommerceContext> options) : base (options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefProductType>().HasMany(z => z.Products).WithOne(y => y.RefProductType).HasForeignKey(x => x.ProductTypeId);
            modelBuilder.Entity<Order>().HasMany(z => z.OrderItems).WithOne(y=>y.Order).HasForeignKey(x=>x.OrderId);
            modelBuilder.Entity<Order>().HasMany(z => z.Shipments).WithOne(y => y.Order).HasForeignKey(x => x.OrderId);
            modelBuilder.Entity<Order>().HasMany(z => z.Invoices).WithOne(y => y.Order).HasForeignKey(x => x.OrderId);
            modelBuilder.Entity<Customer>().HasMany(z => z.Orders).WithOne(y=>y.Customer).HasForeignKey(x=>x.CustomerId);
            modelBuilder.Entity<OrderItem>().HasOne(z => z.Product);
            modelBuilder.Entity<Order>().HasOne(z => z.Customer);
            modelBuilder.Entity<Order>().HasMany(z => z.Invoices);
            modelBuilder.Entity<Order>().HasMany(z => z.Shipments);
            modelBuilder.Entity<Shipment>().HasOne(z => z.Order);
            modelBuilder.Entity<Invoice>().HasOne(z => z.Order);
            modelBuilder.Entity<Invoice>().HasMany(z => z.Shipments);
            modelBuilder.Entity<Product>().HasOne(z => z.RefProductType);

            //modelBuilder.Seed();

        }


        public DbSet<Product> Products { get; set; }
        public DbSet<RefProductType> RefProductTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Invoice> Invoices { get; set; }

    }
}
