﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace webAPI.Models
{
    public class OrderItem
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double RMANumber { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        [JsonIgnore]
        public virtual Order Order { get; set; }
        [JsonIgnore]
        public virtual Product Product { get; set; }

    }
}
